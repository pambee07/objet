const spiderVerseMovie = {
  title: "Spider-Man: Across the Spider-Verse",
  year: 2023,
  director: "Joaquim Dos Santos, Kemp Powers, Justin K. Thompson",
  mainActors: ["Shameik Moore", "Hailee Steinfeld", "Jake Johnson"],
  isFavorite: true,
  rating: 4.8,
  duration: 0,
  setDuration: function (hours, minutes) {
    // Convertir la durée en minutes
    const totalMinutes = hours * 60 + minutes;

    // Sauvegarder la durée dans la propriété duration
    this.duration = totalMinutes;
    console.log(this.duration);
  },
};
spiderVerseMovie.setDuration(2, 20);
